<?php

namespace SCTeam\FreshsalesLaravel;

use Illuminate\Support\ServiceProvider;

/**
 * Class SCTeamServiceProvider
 * @package SCTeam\FreshsalesLaravel
 */
class SCTeamServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/freshsales.php' => config_path('freshsales.php'),
        ], 'config');

        $this->app->bind('freshsales', function ($app) {
            return new FreshsalesService($app['config']['freshsales']);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function registerPackage()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/freshsales.php', 'freshsales');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['freshsales'];
    }
}