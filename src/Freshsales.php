<?php

namespace SCTeam\FreshsalesLaravel;

use Illuminate\Support\Facades\Facade;

/**
 * Class Freshsales
 *
 * @package SCTeam\FreshsalesLaravel
 * @see \SCTeam\FreshsalesLaravel\FreshsalesService
 */
class Freshsales extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'freshsales';
    }
}
