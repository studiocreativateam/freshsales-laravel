<?php

namespace SCTeam\FreshsalesLaravel;

use SCTeam\FreshsalesLaravel\Api\Accounts;
use SCTeam\FreshsalesLaravel\Api\Client;
use SCTeam\FreshsalesLaravel\Api\Config;
use SCTeam\FreshsalesLaravel\Api\Contacts;
use SCTeam\FreshsalesLaravel\Api\Deals;
use SCTeam\FreshsalesLaravel\Api\Leads;
use SCTeam\FreshsalesLaravel\Api\Search;

/**
 * Class FreshsalesService
 *
 * @package SCTeam\FreshsalesLaravel
 */
class FreshsalesService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * FreshsalesService constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->client = new Client($config);
    }

    /**
     * @return \SCTeam\FreshsalesLaravel\Api\Client
     */
    public function client()
    {
        return $this->client;
    }

    /**
     * @return \SCTeam\FreshsalesLaravel\Api\Leads
     */
    public function leads()
    {
        return new Leads($this->client);
    }

    /**
     * @return \SCTeam\FreshsalesLaravel\Api\Contacts
     */
    public function contacts()
    {
        return new Contacts($this->client);
    }

    /**
     * @return \SCTeam\FreshsalesLaravel\Api\Accounts
     */
    public function accounts()
    {
        return new Accounts($this->client);
    }

    /**
     * @return \SCTeam\FreshsalesLaravel\Api\Deals
     */
    public function deals()
    {
        return new Deals($this->client);
    }

    /**
     * @return \SCTeam\FreshsalesLaravel\Api\Search
     */
    public function search()
    {
        return new Search($this->client);
    }

    /**
     * @return \SCTeam\FreshsalesLaravel\Api\Config
     */
    public function config()
    {
        return new Config($this->client);
    }
}