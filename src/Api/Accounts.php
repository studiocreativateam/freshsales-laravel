<?php

namespace SCTeam\FreshsalesLaravel\Api;

/**
 * Class Accounts
 * @package SCTeam\FreshsalesLaravel\Api
 */
class Accounts extends Entity
{
    /**
     * @var string
     */
    protected $entityType = 'sales_account';

    /**
     * @var string
     */
    protected $endPoint = '/api/sales_accounts/';
}