<?php

namespace SCTeam\FreshsalesLaravel\Api;

/**
 * Class Deals
 * @package SCTeam\FreshsalesLaravel\Api
 */
class Deals extends Entity
{
    /**
     * @var string
     */
    protected $entityType = 'deal';

    /**
     * @var string
     */
    protected $endPoint = '/api/deals/';
}