<?php

return [
    'domain' => env('FRESHSALES_DOMAIN'),
    'api_key' => env('FRESHSALES_APIKEY'),
    'enable_rate_limit' => env('FRESHSALES_ENABLE_RATE_LIMIT'),
];